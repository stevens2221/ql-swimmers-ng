# QL-swimmers-ng

### fenics-based swimmers simulator ###

- Developers: Roberto Ausas (rfausas@gmail.com)
              Stevens Paz (stevenspsanchez@gmail.com)
              Gustavo Buscaglia (gustavo.buscaglia@gmail.com)

### Intalling fenics and complements through conda: ###
  - Install conda

  - Create environment (for example "swimmersproject")
    ```sh
    conda create -n swimmersproject
    ```
    
  - Activate environment
    ```sh
    conda activate swimmersproject
    ```
    
  - Install fenics and complements
    ```sh
    conda install -c conda-forge python fenics mshr h5py psutil
    pip3 install gmsh meshio==4.4.6
    ```
    
  - If required, install 
    ```sh
    sudo apt install libsuperlu-dist6
    ```
    
  - If istallation of h5py goes wrong
    ```sh
    pip3 install --no-binary=h5py h5py
    ```